import os
import json
from binascii import hexlify
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from dotenv import load_dotenv

load_dotenv()
SECRET_KEY=os.getenv('SECRET_KEY')

def create_params(payload):
    iv = hexlify(os.urandom(8))
    cipher = AES.new(SECRET_KEY[:16].encode(), AES.MODE_CBC, iv=iv)
    params = {
        "iv": iv.decode(),
        "SECRET_KEY": hexlify(cipher.encrypt(pad(SECRET_KEY.encode(), 16))).decode(),
        "token": payload[0],
    }
    for p in payload[1:]:
        s = p.split(':')
        params[s[0]] = s[1]
    return params

def parseResep(s):
    res = json.loads(s)
    parsed = ""
    try:
        for resep in res['resep']:
            parsed += resep['nama']
            parsed += ':'
            parsed += str(resep['jumlah'])
            parsed += ','
        return parsed[:-1] + '\n'
    except:
        return "error" + '\n'
