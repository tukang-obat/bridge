FROM python:3.8-slim-buster
COPY .env .env
COPY requirements.txt requirements.txt
COPY utils.py utils.py
COPY bridge.py bridge.py
RUN pip3 install -r requirements.txt
CMD ["python3", "bridge.py"]