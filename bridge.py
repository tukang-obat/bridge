import json
import requests
import os
import paho.mqtt.client as mqtt
from dotenv import load_dotenv
from utils import create_params, parseResep

load_dotenv()
USERNAME = os.getenv('USERNAME_BROKER')
PASSWORD = os.getenv('PASSWORD_BROKER')
BROKER_ADDRESS = os.getenv('BROKER_ADDRESS')
BROKER_PORT = int(os.getenv('BROKER_PORT'))

def on_message(client, userdata, message):
    payload = message.payload.decode().strip()
    payload = payload.split(',')
    print(payload, message.topic, flush=True)
    try:
        if (message.topic == 'fromMachine/token'):
            url = os.getenv('URL_GET_RESEP')
            x = requests.get(url, params = create_params(payload))
            parsed = parseResep(x.text)
            print(x.text, parsed, flush=True)
            client.publish("toMachine/resep", parsed)
        elif (message.topic == 'fromMachine/success'):
            url = os.getenv('URL_FINALIZE_RESEP')
            x = requests.get(url, params = create_params(payload))
            print(x.text, flush=True)
            #client.publish("toMachine/success", x.text) # no need
    except:
        pass

client = mqtt.Client("Bridge")
client.on_message = on_message
client.username_pw_set(USERNAME, PASSWORD)
client.connect(BROKER_ADDRESS, BROKER_PORT)
client.subscribe('fromMachine/token')
client.subscribe('fromMachine/success')
client.loop_forever()
