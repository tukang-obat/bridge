import json
import requests
import os
import paho.mqtt.client as mqtt
from dotenv import load_dotenv
from utils import create_params

load_dotenv()
USERNAME = os.getenv('USERNAME_BROKER')
PASSWORD = os.getenv('PASSWORD_BROKER')
BROKER_ADDRESS = os.getenv('BROKER_ADDRESS')
BROKER_PORT = int(os.getenv('BROKER_PORT'))

def on_message(client, userdata, message):
    print(message.payload)
    raise Exception
    

while True:
    msg = input('msg: ')
    menu = input('1. fromMachine/token\n2. fromMachine/success\n> ')
    client = mqtt.Client("test")
    client.on_message = on_message
    client.username_pw_set(USERNAME, PASSWORD)
    client.connect(BROKER_ADDRESS, BROKER_PORT)
    client.subscribe('toMachine/resep')
    client.subscribe('toMachine/success')
    client.on_message = on_message
    if (menu == '1'):
        client.publish('fromMachine/token', msg)
    else:
        client.publish('fromMachine/success', msg)
    try:
        while True:
            client.loop()
    except:
        pass
    client.disconnect()

# kalau ke token, payloadnya token. contoh: 753672
# kalau ke sukses, tambahkan obatnya. contoh: 753672,A:4,B:2,C:1,D:3